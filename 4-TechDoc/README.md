4. Documentation of Specific Fields of Study
============================================
This is a sample collection of examples that show my applied knowledge in various fields. See below for the details behind each document.

Computer Technology:

--------------------

* Inquiry Paper: a short paper explaning common mistakes people make when handling their laptops and quick fixes to take care of laptops more carefully

Database Technology:

--------------

* Examples of SQL Statements from Class Exercises

Programming:

------------

* OrderException.java: a program snippet that throws an exception in PizzaOrder5.java when a person orders more than 5 pizzas (Java)
* PizzaOrder5.java: a program that records pizza up to 5 pizza orders and places them into a text file (Java)
* PriceException.java: a program snippet that throws an exception in PizzaOrder5.java when a person presses order but does not make any selection (Java)