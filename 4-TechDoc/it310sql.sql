/*Create Statements*/

CREATE TABLE ML_BOOK (
	ML_ISBN AUTOINCREMENT(1,1) PRIMARY KEY,
	ML_TITLE VARCHAR (50)
);

CREATE TABLE ML_PERSON (
	ML_PERSON_ID INTEGER PRIMARY KEY,
	ML_FIRSTNAME VARCHAR (50),
	ML_LASTNAME VARCHAR (50)
);

CREATE TABLE ML_RENTAL (
	ML_ISBN AUTOINCREMENT(1,1),
	ML_PERSON_ID INTEGER,
	ML_RENTALDATE DATETIME,
	CONSTRAINT ML_RENTAL_PK PRIMARY KEY (ML_ISBN, ML_PERSON_ID),
	CONSTRAINT ML_ISBN_FK FOREIGN KEY (ML_ISBN) REFERENCES ML_BOOK (ML_ISBN),
	CONSTRAINT ML_PERSON_ID_FK FOREIGN KEY (ML_PERSON_ID) REFERENCES ML_PERSON (ML_PERSON_ID)
);

/*More Create Statements*/

CREATE TABLE ML_DEPARTMENT (
	ML_DEPT_ID INTEGER PRIMARY KEY,
	ML_DEPTNAME VARCHAR (50),
	ML_BUDGETCODE CHAR (4),
	CONSTRAINT ML_BUDGETCODE_CHK CHECK (ML_BUDGETCODE IN ('ABC1', 'DEF2', 'GHI3')),
	ML_BUILDINGNUMBER VARCHAR (4)
);

CREATE TABLE ML_EMPLOYEE (
	ML_EMPLOYEE_ID AUTOINCREMENT(1,1) PRIMARY KEY,
	ML_FNAME VARCHAR (50),
	ML_LNAME VARCHAR (50),
	ML_DEPT_ID INTEGER,
	ML_PHONE VARCHAR (20),
	ML_EMAIL VARCHAR (50),
	CONSTRAINT ML_DEPT_ID_FK FOREIGN KEY (ML_DEPT_ID) REFERENCES ML_DEPARTMENT (ML_DEPT_ID) ON UPDATE CASCADE ON DELETE NO ACTION
);

/*Alter Statements*/

ALTER TABLE ML_EMPLOYEE
ADD ML_ZIP_CODE VARCHAR(10);

ALTER TABLE ML_EMPLOYEE
ADD CONSTRAINT ML_DEPT_ID_FK FOREIGN KEY (ML_DEPT_ID) REFERENCES ML_DEPARTMENT (ML_DEPT_ID);

/*Insert, Update, and Delete Statements*/

ML_DEPARTMENT (ML_DEPT_ID, ML_DEPTNAME, ML_BUDGETCODE, ML_BUILDINGNUMBER)

INSERT INTO ML_DEPARTMENT (ML_DEPT_ID, ML_DEPTNAME, ML_BUDGETCODE, ML_BUILDINGNUMBER)
VALUES (1, 'IT Department', 'ABC1', 1000);

ML_EMPLOYEE (ML_EMPLOYEE_ID, ML_FNAME, ML_LNAME, ML_DEPT_ID, ML_PHONE, ML_EMAIL)

INSERT INTO ML_EMPLOYEE (ML_EMPLOYEE_ID, ML_FNAME, ML_LNAME, ML_DEPT_ID, ML_PHONE, ML_EMAIL)
VALUES (1, 'Marian', 'Lumba', 1, '702-685-3767', 'mml85366@marymount.edu');

UPDATE ML_EMPLOYEE
SET ML_PHONE = '703-207-1692'
WHERE ML_PHONE = '702-685-3767';

DELETE FROM ML_EMPLOYEE
WHERE ML_FNAME = 'Marian';

/*Insert and Select Statements*/

INSERT INTO ML_CUSTOMER ( ML_CustomerID, ML_LastName, ML_FirstName, ML_Phone, ML_Email )
VALUES (8, 'Lumba', 'Marian', '123-456-7890', 'mml85366@marymount.edu')

SELECT ML_CustomerID, ML_LastName, ML_FirstName, ML_Phone, ML_Email
FROM ML_CUSTOMER

SELECT ML_LastName, ML_Phone
FROM ML_CUSTOMER

SELECT ML_Description, ML_Cost
FROM ML_ITEM
ORDER BY ML_Cost DESC;

/*Select Distinct Statements*/

SELECT DISTINCT ML_ItemNumber
FROM ML_PURCHASE_ITEM
ORDER BY ML_ItemNumber;

SELECT ML_LastName, ML_FirstName
FROM ML_CUSTOMER
WHERE ML_FirstName IN ('Fred', 'Pamela', 'Ricardo');

SELECT ML_Email
FROM ML_CUSTOMER
WHERE ML_Phone NOT IN ('555-236-1095');

SELECT DISTINCT ML_InvoiceNumber
FROM ML_PURCHASE_ITEM
WHERE ML_RetailPrice BETWEEN 100 AND 500;

/*Select Statement with Wildcard*/

SELECT ML_LastName, ML_FirstName, ML_Phone
FROM ML_CUSTOMER
WHERE ML_LastName LIKE 'P%' OR ML_FirstName LIKE 'P%';

/*More Select Statements*/

SELECT ML_OwnerID, ML_LastName, ML_FirstName, ML_Phone, ML_Email
FROM ML_PET_OWNER
ORDER BY ML_LastName;

SELECT ML_Breed, ML_Type
FROM ML_PET
WHERE ML_Type = 'Cat'
ORDER BY ML_Breed, ML_Type;

SELECT ML_Breed, ML_Type, ML_DOB
FROM ML_PET
WHERE ML_OwnerID = 3 AND ML_Type = 'Dog';

SELECT DISTINCT ML_Breed
FROM ML_PET
ORDER BY ML_Breed;

SELECT ML_Name, ML_Breed, ML_Type
FROM ML_PET
WHERE ML_Type NOT IN ('Dog', 'Cat', 'Fish');

SELECT ML_Breed, ML_Type, ML_DOB
FROM ML_PET
WHERE ML_Type = 'Dog' AND ML_Breed = 'Std. Poodle';

/*Count and Average Statements*/

SELECT Count(ML_CustomerID) AS Expr1
FROM ML_CUSTOMER;

SELECT COUNT (DISTINCT ML_ArtistName)
FROM ML_ITEM;

SELECT AVG (ML_Cost)
FROM ML_ITEM
WHERE ML_ArtistName = 'Baker';

/*Average and Count Statements*/

SELECT ML_ArtistName, AVG (ML_Cost)
FROM ML_ITEM
GROUP BY ML_ArtistName
HAVING AVG (ML_Cost) > 100;

SELECT ML_State, COUNT(ML_InvoiceNumber)
FROM ML_PURCHASE
GROUP BY ML_State
HAVING COUNT (ML_InvoiceNumber) >= 2;

/*Minimum and Select Statements*/

SELECT C.ML_LastName, C.ML_FirstName, C.ML_Phone, MIN (P.ML_PreTaxAmount)
FROM ML_CUSTOMER AS C INNER JOIN ML_PURCHASE AS P ON C.ML_CustomerID = P.ML_CustomerID
GROUP BY C.ML_LastName, C.ML_FirstName, C.ML_Phone
HAVING MIN (P.ML_PreTaxAmount) > 200;

SELECT ML_LastName, ML_FirstName
FROM ML_CUSTOMER
WHERE ML_CustomerID IN
(SELECT ML_CustomerID
FROM ML_PURCHASE
WHERE ML_PreTaxAmount > 200);

SELECT C.ML_LastName, C.ML_FirstName
FROM (ML_CUSTOMER AS C INNER JOIN ML_PURCHASE AS P ON C.ML_CustomerID = P.ML_CustomerID)
INNER JOIN ML_PURCHASE_ITEM AS PI ON P.ML_InvoiceNumber = PI.ML_InvoiceNumber
WHERE PI.ML_RetailPrice > 200;

/*Views*/

SELECT C.ML_CustomerID, C.ML_LastName, C.ML_FirstName, P.ML_InvoiceNumber, P.ML_PurchaseDate, P.ML_PreTaxAmount
FROM (ML_CUSTOMER AS C INNER JOIN ML_PURCHASE AS P ON C.ML_CustomerID = P.ML_CustomerID)
WHERE P.ML_PreTaxAmount > 200;

SELECT V.ML_LastName, V.ML_FirstName
FROM ML_VIPCUSTOMERVIEW V