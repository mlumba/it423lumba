Senior IT Portfolio
===================
Last Updated: November 25, 2013

This is the senior IT portfolio for Marian Lumba, which contains the following contents:

1. Resume
2. Personal Statement: Answers the following questions - "Why did you choose the one (or more) IT specialty you are in?" and "Why would you consider the Washington DC metro are a good place to study and work?"
3. Senior Activities: IT 400 (Internship) and IT 489 (Capstone)
4. Technical Documents: These documents display evidence of applied knowledge in different areas
5. Soft Skills: These documents display evidence of soft skills such as written communications and informational packets.
6. Extra Curricular Activities: These documents display evidence of the extracurricular activities I have participated in.
7. Awards and Honors: A word document containing information on the various awards, honors, and scholarships I have received during my time at Marymount University.
8. Career Plan: A word document containing information on future career plans.